/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Fichero: Ejercicio0609.java
 *
 * @date 10-feb-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejercicio0609 {

  final static String FILENAME = "config.xml";

  public static Document abrirXML()
          throws ParserConfigurationException, SAXException, IOException {
    File fXmlFile = new File(FILENAME);
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    Document doc = dBuilder.parse(fXmlFile);
    doc.getDocumentElement().normalize();
    return doc;
  }

  public static String leerString() throws IOException {
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    String linea;
    linea = buffer.readLine();
    return linea;
  }

  public static String buscarTag(Document doc, String tagname) throws IOException {
    String text;
    NodeList nList = doc.getElementsByTagName("configuracion");

    Node nNode = nList.item(0);
    Element eElement = (Element) nNode;
    text = eElement.getElementsByTagName(tagname).item(0).getTextContent();
    return text;

  }

  public static void main(String[] args) throws Exception {

    Document doc;

    String usuario;
    String contasenya;

    String usuarioxml = "";
    String contrasenyaxml = "";

    doc = abrirXML();

    System.out.println("CONTROL DE ACCESO ");

    System.out.print("Usuario: ");
    usuario = leerString();

    System.out.print("Constaseña: ");
    contasenya = leerString();

    usuarioxml=buscarTag(doc,"user");
    contrasenyaxml=buscarTag(doc,"password");

    if (usuario.equals(usuarioxml) && contasenya.equals(contrasenyaxml)) {
      System.out.println("Acceso Permitido. ");
    } else {
      System.out.println("Acceso no permitido ");
    }

  }

}

/* EJECUCION:
CONTROL DE ACCESO 
Usuario: admin
Constaseña: 123
Acceso Permitido.
 * */

/* Fichero: config.xml
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<configuracion>
      <user>admin</user>
      <password>123</password>
</configuracion>
 */
